using System;

public class Objects
{
	public static void Main(string[] args)
	{
		Person p = new Person("luke", 98);

		// Calling getters
		Console.WriteLine(
			String.Format("Name: {0} Age: {1}", p.Name, p.Age));
	}
}

class Person
{
	public string Name { get; set; } // Property, getter and setter
	public int Age { get; set; } // Property, getter and setter

	public Person(string name, int age)
	{
		this.Name = name;
		this.Age = age;
	}
}

