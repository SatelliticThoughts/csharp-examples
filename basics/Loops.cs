using System;

public class Loops
{
	public static void Main(string[] args)
	{
		bool running = true;
		int num = 0;

		// while loop
		while(running)
		{
			num += 1;
			if(num > 5)
			{
				running = false;
			}
		}

		// for loop
		for(int i = 1; i < 13; i++)
		{
			Console.WriteLine(
				String.Format("256 * {0} = {1}", i, 256*i));
		}

		double[] numbers = {1.22121212, 0.001001001101, 909, 102.102};

		foreach(double n in numbers)
		{
			Console.WriteLine(n);
		}
	}
}

