using System;

public class Print
{
	public static void Main(String[] args)
	{
		// Hello World example
		Console.WriteLine("Hello World!");

		// Printing variable example
		string text = "This is a test";
		Console.WriteLine(text);

		// Printing multiple variables
		string name = "luke";
		int age = 55;
		Console.WriteLine(String.Format(
				"My name is {0} and i am {1} years old", 
				name, age));
	}
}

