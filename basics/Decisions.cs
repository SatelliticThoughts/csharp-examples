using System;

public class Decisions
{
	public static void Main(string[] args)
	{
		// Basic if statement.
		if(true)
		{
			Console.WriteLine("This will show");
		}

		// Basic if/else statement.
		if(false)
		{
			Console.WriteLine("This will NOT show");
		}
		else
		{
			Console.WriteLine("This will show");
		}

		// Basic if/else if/else statement
		if(false)
		{
			Console.WriteLine("This will NOT show");
		}
		else if(true)
		{
			Console.WriteLine("This will show");
		}
		else
		{
			Console.WriteLine("This will NOT show");
		}

		// &&, and
		if(true && true)
		{
			Console.WriteLine("This will show");
		}

		if(true && false)
		{
			Console.WriteLine("This will NOT show");
		}

		if(false && false)
		{
			Console.WriteLine("This will NOT show");
		}

		// ||, or
		if(true || true)
		{
			Console.WriteLine("This will show");
		}

		if(true || false)
		{
			Console.WriteLine("This will show");
		}

		if(false || false)
		{
			Console.WriteLine("This will NOT show");
		}

		int x = 1200;
		int y = 98765;

		// Equals
		if(x == y)
		{
			Console.WriteLine("If x equals y this will show");
		}

		// Not Equal
		if(x != y)
		{
			Console.WriteLine(
				"If x does NOT equal y this will show");
		}

		// Greater Than
		if(x > y)
		{
			Console.WriteLine(
				"If x is bigger than y this will show");
		}

		// Less Than
		if(x < y)
		{
			Console.WriteLine(
				"If x is smaller than y this will show");
		}
	}
}

