using System;

public class Variables
{
	public static void Main(String[] args)
	{
		// Integers
		int x = 450;
		int y = 90;
		
		Console.WriteLine(String.Format(
				"Integer Calculations, x = {0}, y = {1}", 
				x, y));

		Console.WriteLine(String.Format("x + y = {0}", x + y)); // Addition
		Console.WriteLine(String.Format("x - y = {0}", x - y)); // Subtraction
		Console.WriteLine(String.Format("x * y = {0}", x * y)); // Multiplication
		Console.WriteLine(String.Format("x / y = {0}", x / y)); // Division
		Console.WriteLine(String.Format("x % y = {0}", x % y)); // Modulus

		// Floating point numbers
		float a = 8.41879f;
		float b = 4.907653f;

		Console.WriteLine(String.Format(
				"\nFloating point Calculations, x = {0}, y = {1}",
				a, b));

		Console.WriteLine(String.Format("a + b = {0}", a + b)); // Addition
		Console.WriteLine(String.Format("a - b = {0}", a - b)); // Subtraction
		Console.WriteLine(String.Format("a * b = {0}", a * b)); // Multiplication
		Console.WriteLine(String.Format("a / b = {0}", a / b)); // Division
		Console.WriteLine(String.Format("a % b = {0}", a % b)); // Modulus

		// Strings
		Console.WriteLine("\nString Manipulation");

		string hello = "Hello";
		Console.WriteLine(hello);

		hello = hello + " world ";
		Console.WriteLine(hello);

		Console.WriteLine(hello.Substring(3, 7));

		// Arrays
		int[] numbers = new int[10];
		for(int i = 0; i < numbers.Length; i++)
		{
			numbers[i] = i * 354;
		}

		Console.WriteLine(numbers[4]);
	}
}

