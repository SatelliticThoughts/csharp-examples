using System;

public class Methods
{
	public static void Main(String[] args)
	{
		// Calling Methods
		SayHello();
		Console.WriteLine(GetWord());
		DoubleNumber(5039578);
		Console.WriteLine(AddNumbers(124894, 78353));
	}

	// Method with no return statement or parameters
	public static void SayHello()
	{
		Console.WriteLine("hello");
	}

	// Method returning string
	public static string GetWord()
	{
		return "cat";
	}

	// Method with single parameter
	public static void DoubleNumber(int x)
	{
		Console.WriteLine(x * 2);
	}

	// Method with multiple parameters and a return statement
	public static int AddNumbers(int x, int y)
	{
		return x + y;
	}
}

