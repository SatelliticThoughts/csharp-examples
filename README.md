# csharp-examples

csharp-examples contains code to demonstrate various things in C sharp.

## Set up

After cloning or downloading, most programs should be a single file that can be compiled with mcs and run like any other executable, unless otherwise stated.

```
mcs 'filename.cs' # compile file
./'filename' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
